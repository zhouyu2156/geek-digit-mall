import axios from 'axios'
import router from '@/router'
import { ElMessage } from 'element-plus'

export const baseURL = 'http://127.0.0.1:8000/'

const instance = axios.create({
    baseURL,
    timeout: 100000,
    // 设置请求头
    headers: {
        'Content-Type': 'application/json'
    }
});


instance.interceptors.request.use(
    (config) => {
    //   const userStore = useUserStore()
    //   if (userStore.token) {
    //     config.headers.Authorization = userStore.token
    //   }
      return config
    },
    (err) => Promise.reject(err)
  )
  
  instance.interceptors.response.use(
    (res) => {
      if (res.data.status === 200) {
        ElMessage({ 
            type: "success",
            message: '请求成功!'
        })
        return res;
      }
      ElMessage({ message: res.data.message || '服务异常', type: 'error' })
      return Promise.reject(res.data)
    },
    (err) => {
      ElMessage({ message: err.response.data.message || '服务异常', type: 'error' })
      console.log(err)
      if (err.response?.status === 400) {
        router.push('/login')
        ElMessage({ 
            type: "info",
            message: '请登录!'
        })
      }
      return Promise.reject(err)
    }
  )

export * from './goods'
export * from './user'

export default instance








