import instance from "@/api/index";

export const send_verify_code = (email: string) => instance.post('/user/send_verify_code/', {
    email
})