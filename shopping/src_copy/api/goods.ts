import instance from "./index";

// 获取本地推荐商品信息
export const get_local_recommend = () => instance.get('get_local_taobao_recommend/')
// 根据链接获取商品详情
export const getGoodsDetail = (link: string) => instance.post(`/goods/get_goods_detail/`, { link: link })

// 获取分类商品列表
export const getCategoryGoodsList = (cate: string) => instance.get(`/get_local_category_goods_list/?kw=${cate}`)



