import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue';
import NotFound from '@/views/NotFound.vue';


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Login.vue')
    },
    {
      path: '/detail',
      name: 'detail',
      component: () => import('@/views/Detail.vue')
    },
    {
      path: '/center',
      name: 'center',
      component: () => import('@/views/Center.vue')
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import('@/views/Cart.vue')
    },
    { 
      path: '/:pathMatch(.*)*', 
      name: 'NotFound', 
      component: NotFound 
    },
  ]
})

export default router
