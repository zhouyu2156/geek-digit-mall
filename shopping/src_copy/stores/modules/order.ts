import { ref } from 'vue'
import { defineStore } from 'pinia'


export const useOrderStore = defineStore('order', () => {
    const cart = ref(1)
    
    return { cart }
}, {
    persist: true // 开启数据持久化
})