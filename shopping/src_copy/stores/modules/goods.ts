import { ref, computed } from 'vue'
import { defineStore } from 'pinia'


export const useGoodsStore = defineStore('goods', () => {
  const goods = ref(0)
  
  return { goods }
}, {
  persist: true // 持久化
})
