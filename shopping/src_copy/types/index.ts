
export type User = {
    id: number,
    email: string,
    password: string,
    nickname: string,
    sex: number,
    phone: string,
    address: string,
    create_date: string,
    balance: number,
    status: number
}

export type Order = {
    id: number,
    goods_id: string,
    user_email: string,
    goods_name: string,
    user_name: string,
    goods_url: string,
    goods_price: number,
    goods_count: number,
    post_address: string,
    logistics: string,
    status: number
}

export type Goods = {
    id: string,
    name: string,
    price: number,
    desc: string,
    total: number,
    url: string,
    url1: string,
    url2: string,
    url3: string,
    business: string,
    category: string,
    status: number
}
export type GoodsEasy = {
    id?: number | string,
    url: string,
    title: string,
    img_url: string,
    price: number | string,
    shop_name?: string,
    sold_count?: string
}

export interface RecommendGoods {
    id: number,
    url: string,
    img_url: string,
    title: string,
    price: number
}







