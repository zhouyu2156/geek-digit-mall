import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import Detail from '@/views/Detail.vue'
import Login from '@/views/Login.vue'
import User from '@/views/User.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
      /*
      * 1、首页
      * 2、商品详情页
      * 3、注册、登录
      * 4、个人中心
      *
      * */
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/detail',
      name: 'detail',
      component: Detail
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/user',
      name: 'user',
      component: User
    },
  ]
})

router.beforeEach((to, from) => {
  if (to.name === 'user') {
    const auth = sessionStorage.getItem("auth");
    if (auth) {
      return true;
    } {
      alert("请先登录!");
      window.location = '/login'
    }
  }
})

export default router
