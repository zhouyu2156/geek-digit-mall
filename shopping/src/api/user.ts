import instance from '@/api'

export const send_verify_code_server = (email: string) => instance.post('/user/send_verify_code/', { email })
export const register = (data) => instance.post('/user/register/', data)
export const login = (data) => instance.post('/user/login/', data)
export const getUserInformation = (data: { email: string, password: string }) => instance.post('/user/information/', data)
export const updateUserInformationServer = (data) => instance.post('/user/update_user/', data)