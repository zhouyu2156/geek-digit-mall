
import instance from "@/api/index";

export const get_local_recommend = () => instance.get('/get_local_taobao_recommend/')
export const get_local_category_goods_list = (cate: string) => instance.get(`/get_local_category_goods_list/?kw=${cate}`)

// 订单创建
export const generateOrder = (data) => instance.post('/order/create/', data)
// 获取用户订单
export const getOrderList = (data) => instance.post('/order/order_list/', data)




