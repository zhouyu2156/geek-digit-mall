import axios from 'axios'
export const baseURL = 'http://127.0.0.1:8000/'

const instance = axios.create({
    baseURL,
    timeout: 100000,
    // 设置请求头
    headers: {
        'Content-Type': 'application/json'
    }
});


export * from './goods'
export * from './user'

export default instance








