/*
*   @Author: BestRivenNA
*   @Date: 2023-12-30 15:13:13
*   @FileName: index
*/

export type Goods = {
    id: number,
    img_url: string,
    price: string,
    shop_name: string,
    sold_count: string,
    title: string,
    url: string
}

