import { ref } from 'vue'
import { defineStore } from 'pinia'


const useUserStore = defineStore('user', () => {
    const user = ref({ email: '', password: '', auth: false })
    const getUser = () => user;
    const clearUser = () => {
        user.value = { email: '', password: '', auth: false };
    }
    return { user, getUser, clearUser }
}, {
    persist: true // 开启数据持久化
})

export default useUserStore