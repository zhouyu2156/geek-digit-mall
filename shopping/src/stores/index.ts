import { createPinia } from 'pinia'
import persist from 'pinia-plugin-persistedstate'

const pinia = createPinia().use(persist)


export * from './modules/goods'
export * from './modules/user'

export default pinia