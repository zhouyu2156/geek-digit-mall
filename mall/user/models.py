from django.db import models

class User(models.Model):
    # verbose_name 是该字段的注释信息、null表示该字段是否可以为空
    id = models.AutoField(auto_created=True, primary_key=True, verbose_name="用户ID")
    email = models.CharField(max_length=16, unique=True, null=False, verbose_name="用户邮箱")
    password = models.CharField(max_length=32, null=False, verbose_name="用户密码")
    nickname = models.CharField(max_length=16, null=True, blank=True, verbose_name="用户昵称")
    sex = models.SmallIntegerField(default=1, verbose_name="用户性别")
    phone = models.CharField(max_length=11, verbose_name="用户手机号")
    address = models.CharField(max_length=50, verbose_name="用户居住地址")
    create_date = models.DateTimeField(auto_now=True)
    balance = models.IntegerField(default=10000, verbose_name="钱包余额")
    status = models.SmallIntegerField(default=0, verbose_name="用户登录状态")

    def __str__(self):
        return f"<User {self.email}>"

    class Meta:
        db_table = 'user'



