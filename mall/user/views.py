import random
from django.http import HttpResponse, JsonResponse
import json
from .models import User
from django.views import View
from django.core.mail import send_mail
from mall.settings import EMAIL_FROM
from django.views.decorators.http import require_http_methods


# 在这里编写你的视图函数

def index(request):
    return HttpResponse('<h1 style="color: orangered;text-align:center;">Hell World! User 模块.</h1>')


@require_http_methods(["POST"])  # 限制只接收 POST 请求
def send_verify_code(request):
    # 一、前端数据提交的方式好几种, 所以后端需要捋清楚怎么提取前端发送来的数据
    # (1) 若 "Content-Type": "application/json"
    #        通过 json.loads(request.body.decode('utf-8')) 前端提交的json数据转化为python的字典对象
    # (2) 若 "Content-Type": "x-www-form-urlencoded"
    #        email = request.POST['email'] 数据存放在POST对象里

    # 测试(1)：第一种方式获取前端提交 json 格式的数据
    # request.body 是字节类型的字符串, 需要解码处理为 'utf-8' 的编码数据
    json_data = json.loads(request.body.decode('utf-8'))
    print(json_data)

    # 测试(2)：
    print(request.POST)
    # 因为我的前端设置的"Content-Type": "application/json" 所以数据在 body 身上, 反之, django会将前端表单提交的数据存放在 request.POST 对象上

    # 所以获取前端发送来的邮箱数据如下
    whose_email = json.loads(request.body.decode('utf-8')).get('email')
    print(whose_email)

    # 二、开始生成随机验证码字符
    string = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    # 生成4随机验证码
    verify_code = ''
    for i in range(4):
        char = string[random.randint(0, 62)]
        verify_code += char
    print("生成的验证码是: ", verify_code)
    # 定义发送内容
    subject = '极客数码商城网站注册验证码'       # 邮件标题
    message = f'你的验证码是: {verify_code}'  # 邮件内容
    email_to = whose_email                  # 收件人
    send_status = None
    try:
        send_status = send_mail(
            subject=subject,
            message=message,
            from_email=EMAIL_FROM,
            recipient_list=[email_to],  # 可以批量定义接收人邮箱
        )
        print("发送状态: ", send_status)  # 1表示成功, 0表示失败
    finally:
        if send_status:
            return JsonResponse({'status': 200, 'msg': '验证码发送成功!', 'verify_code': verify_code})
        else:
            return JsonResponse({'status': 400, 'msg': '验证码发送失败!'})

# 发送邮箱验证码
def send_verify_code1(request):
    # (1) Content-Type: application/json
    #       json.loads(request.body).get('email')
    # (2) x-www-form-urlencoded
    #       email = request.POST['email']
    if request.method == 'POST':
        # 获取用户邮箱
        whose_email = json.loads(request.body).get('email')
        string = 'abcdefghijklmnopqrstuvwxyz1234567890'
        # 生成4随机验证码
        verify_code = ''
        print(verify_code)
        for i in range(4):
            char = string[random.randint(0, 35)]
            verify_code += char
        # 将验证码保存到用户会话或数据库中，用于后续验证
        request.session['verify_code'] = verify_code
        # 发送邮件
        subject = 'Your Verification Code'  # 邮件标题
        message = f'Your verification code is: {verify_code}'  # 邮件内容
        # email_from = EMAIL_FROM  # 来信人
        email_to = whose_email  # 收件人

        send_status = send_mail(subject=subject, message=message, from_email='', recipient_list=[email_to])
        print("发送状态: ", send_status)
        if send_status:
            return HttpResponse(json.dumps({
                'status': 200,
                'msg': '邮件发送成功, 请查收!',
                'verify_code': verify_code
            }, ensure_ascii=False))
        else:
            return HttpResponse(json.dumps({
                'status': 400,
                'msg': '邮件发送失败,请检查!'
            }, ensure_ascii=False))
    else:
        return HttpResponse(json.dumps({
            'status': 400,
            'msg': '请求方式错误!'
        }))


def register(request):
    # 获取前端提交的表单数据
    email = json.loads(request.body).get('email')
    password = json.loads(request.body).get('password')
    try:
        User.objects.create(email=email, password=password, address='湖南省长沙市岳麓区')
        # 已经进行前端本地校验, 无需再进行校验验证码
        return HttpResponse(json.dumps({
            'status': 200,
            'msg': '亲、注册成功, 请登录!'
        }))
    except Exception as e:
        print(e)
    return JsonResponse({'status': 400, 'msg': '该账号已被人注册!'})


def login(request):
    print(request.body)
    # 获取前端提交的表单数据
    email = json.loads(request.body).get('email')
    password = json.loads(request.body).get('password')
    try:
        user = User.objects.get(email=email)
        user.status = 1
        user.save()
        if user.password == password:
            return HttpResponse(json.dumps({
                'email': email,
                'password': password,
                'status': 200,
                'msg': '登陆成功!',
                'auth': True
            }))
        else:
            return HttpResponse(json.dumps({
                'status': 400,
                'msg': '请检查密码是否输入正确!'
            }))
    except Exception as e:
        print(e)
        return HttpResponse(json.dumps({
            'status': 400,
            'msg': '登录失败, 请检查账户是否存在!'
        }))


# 获取用户个人信息
def get_information(request):
    email = json.loads(request.body).get('email')
    password = json.loads(request.body).get('password')
    user = User.objects.get(email=email)
    try:
        if user.password == password:
            return HttpResponse(json.dumps({
                'status_code': 200,
                'email': user.email,
                'password': user.password,
                'nickname': user.nickname,
                'phone': user.phone,
                'balance': user.balance,
                'status': user.status,
                'address': user.address
            }))
    except Exception as e:
        print(e)
        return HttpResponse(json.dumps({'status': 400, 'msg': '服务器错误!'}))


# 更新用户信息
def update_user(request):
    email = json.loads(request.body).get('email')
    user = User.objects.get(email=email)
    user.nickname = json.loads(request.body).get('nickname')
    user.address = json.loads(request.body).get('address')
    user.password = json.loads(request.body).get('password')
    user.phone = json.loads(request.body).get('phone')
    user.save()
    return HttpResponse(json.dumps({'status': 200, 'msg': '信息更改成功!'}))


# 退出功能 - 清除 session
def logout(request):
    request.session['email'] = None
    return JsonResponse({'status': 200, 'msg': '退出成功!'})
