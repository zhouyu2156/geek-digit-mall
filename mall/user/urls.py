from django.urls import path
from .views import *

urlpatterns = [
    path('', index),
    path('send_verify_code/', send_verify_code),
    # 用户登录权限相关处理
    path('register/', register),
    path('login/', login),
    path('information/', get_information),
    path('update_user/', update_user)
]

