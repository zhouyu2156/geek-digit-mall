import json
import random
import time
from django.http import HttpResponse
from lxml import etree
from selenium import webdriver
from constant import script


def get_recommend_goods_list(request):
    """:description 爬取淘宝首页推荐数据使用"""
    browser = webdriver.Chrome()
    browser.get('https://www.taobao.com')
    browser.execute_script(script)
    # 获取淘宝推荐链接返回给前端
    # 推荐商品基础 xpath: //a[@class="item-link"]
    time.sleep(3)
    # 获取网页源码
    tree = etree.HTML(browser.page_source)
    # 获取内容
    goods_base_link_list = tree.xpath("//div[@class='tb-recommend-content-item']/a[@class='item-link']/@href")[:30]
    # 将商品链接拼接成完整链接
    goods_link_list = ['https:' + item for item in goods_base_link_list]
    # 商品图片地址列表
    goods_base_img_url_list = tree.xpath('//a[@class="item-link"]/div[@class="img-wrapper"]/img/@src')[:30]
    goods_img_url_list = ['https:' + item for item in goods_base_img_url_list]
    # 商品标题列表
    goods_title_list = tree.xpath('//a[@class="item-link"]/div[@class="info-wrapper"]/div[@class="title"]/text()')[:30]
    # 商品价格列表
    goods_base_price_list = tree.xpath(
        '//a[@class="item-link"]/div[@class="price-wrapper"]/span[@class="price-value"]/text()')[:30]
    goods_price_list = [float(item) for item in goods_base_price_list]
    taobao = []
    for i in range(30):
        obj = {
            'id': i,
            'url': goods_link_list[i],
            'title': goods_title_list[i],
            'img_url': goods_img_url_list[i],
            'price': goods_price_list[i]
        }
        taobao.append(obj)
    with open('../taobao.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps({'taobao': taobao}, ensure_ascii=False, indent=2))
    browser.close()
    browser.quit()
    return HttpResponse(json.dumps(taobao))


def get_category_goods_list(request):
    kw = request.GET.get('kw')
    # 获取分类总数
    SUM = 30
    browser = webdriver.Chrome()
    browser.get(f'https://s.taobao.com/search?q={kw}')
    # 找到分类标签、点击
    # btn = browser.find_element(by=By.XPATH, value='//li[contains(@class,"J_Cat")]/a[contains(text(),"手机")]')
    # btn.click()
    browser.execute_script(script)
    time.sleep(5)
    tree = etree.HTML(browser.page_source)
    # 商品链接
    goods_url_list = tree.xpath(
        '//div[contains(@class,"LeftLay--leftContent")]//a[contains(@class,"Card--doubleCardWrapper")]/@href')[:SUM]
    # 商品图片链接
    goods_img_url_list = tree.xpath('//div[contains(@class,"MainPic--mainPicWrapper")]/img/@src')[:SUM]
    # 商品标题列表
    goods_base_title_list = tree.xpath(
        '//div[contains(@class,"LeftLay--leftContent")]//div[contains(@class,"Title--title")]//span//text()')[:SUM]
    goods_title_list = []
    # 拼接标题字符串
    for item in goods_base_title_list:
        tmp = ''
        for segment in item:
            tmp += segment
        goods_title_list.append(tmp)
    # 整数价格列表
    goods_int_price_list = tree.xpath(
        '//div[contains(@class,"Price--priceWrapper")]/span[contains(@class,"Price--priceInt")]//text()')[:SUM]
    # 小数部分价格列表
    goods_float_price_list = tree.xpath(
        '//div[contains(@class,"Price--priceWrapper")]/span[contains(@class,"Price--priceFloat")]//text()')[:SUM]
    # 商品购买实情
    # goods_sold_count_list = tree.xpath('//div[contains(@class,"LeftLay--leftContent")]//span[contains(@class,"Price--realSales")]/text()')[:SUM]
    # 店铺列表
    shop_name_list = tree.xpath('//div[contains(@class,"ShopInfo--shopInfo")]//a/text()')[:SUM]
    categories = []
    for index in range(SUM):
        obj = {
            'id': index,
            'url': goods_url_list[index],
            'img_url': goods_img_url_list[index],
            'title': goods_title_list[index],
            'price': goods_int_price_list[index] + goods_float_price_list[index],
            'shop_name': shop_name_list[index],
            'sold_count': str(random.randint(0, 500)) + '+人付款'
        }
        categories.append(obj)
    with open(f'{kw}.json', 'w', encoding='utf-8') as fp:
        fp.write(json.dumps(categories, ensure_ascii=False, indent=2))
    browser.close()
    browser.quit()
    return HttpResponse(json.dumps(categories))
