import json
from goods.models import Goods


def main():
    offset = 0      # 在生成数据时，将偏移量依次设置为 30、60
    # 将下面json文件依次改为 db 目录下的json文件名进行运行
    with open("手机.json", 'r', encoding='utf-8') as fp:
        recommend_goods_list = json.load(fp=fp)
        print(item for item in recommend_goods_list)
        for index in range(30):
            item = recommend_goods_list[index]
            Goods.objects.create(**{
                'id': item.get('id') + offset,
                'img_url': item.get('img_url'),
                'url': item.get('url'),
                'title': item.get('title'),
                'price': item.get('price'),
                'shop_name': item.get('shop_name'),
                'sold_count': item.get('sold_count'),
                'total': 400,
                'category': '手机'
            })
            print("添加完成\n")
    print("数据添加完成...")
