from django.urls import path
from .views import *

urlpatterns = [
    path('', index),
    path('good_list/', wait_pay, name='good_list'),
    path('create/', generatorOrder),
    path('getOrderOfUser/', getOrderOfUser),
    path('order_list/', get_order_list)
]
