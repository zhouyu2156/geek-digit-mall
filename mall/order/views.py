import numbers
import uuid

from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from user.models import User
from .models import Order
import json
from django.http import JsonResponse
from goods.models import Goods
from django.shortcuts import render
from .utils import alipay_object
from django.conf import settings
from django.http import JsonResponse


# 在这里编写你的视图函数

def index(request):
    return HttpResponse('<h1 style="color: orangered;text-align:center;">Hell World! Order 模块.</h1>')


def generatorOrder(request):
    # 获取前端发送的用户信息
    email = json.loads(request.body).get('email')
    user = User.objects.get(email=email)
    post_address = user.address
    user_name = user.nickname
    # 获取前端发送的商品信息
    goods_id = json.loads(request.body).get('id')
    goods_count = json.loads(request.body).get('goods_count')
    goods_price = json.loads(request.body).get('price')
    img_url = json.loads(request.body).get('img_url')
    goods_title = json.loads(request.body).get('title')
    # 发起支付链接
    return wait_pay(request, goods_id, goods_price, goods_title)
    # 创建订单
    # try:
    #     Order.objects.create(goods=goods_id,
    #                          user=email,
    #                          goods_title=goods_title,
    #                          goods_count=goods_count,
    #                          post_address=post_address,
    #                          goods_price=goods_price,
    #                          logistics="待发货",
    #                          goods_url=img_url,
    #                          user_name=user_name)
    # except Exception as e:
    #     print(e)
    #     return JsonResponse({'status': 400, 'msg': '下单失败!'})
    # return JsonResponse({'status': 200, 'msg': '购买成功!'})


# 用户订单获取
def getOrderOfUser(request):
    email = json.loads(request.body).get('email')
    order_queryset = Order.objects.filter(user=email)
    for item in order_queryset:
        print(item.user)

    return JsonResponse({'status': 200, 'msg': '获取订单成功!'})


def get_order_list(request):
    # 获取订单列表
    email = json.loads(request.body).get('email')
    order_queryset = Order.objects.filter(user=email)
    order_list = []
    for item in order_queryset:
        obj = {
            'goods_price': item.goods_price,
            'goods_count': item.goods_count,
            'post_address': item.post_address,
            'goods_title': item.goods_title,
            'goods_url': item.goods_url,
            'logistics': item.logistics,
            'status': item.status
        }
        order_list.append(obj)
    return HttpResponse(json.dumps({
        'status': 200,
        'order_list': order_list
    }))


def wait_pay(request, goods_id, goods_price, goods_title):
    """:return 返回付款链接 - 等待支付"""
    alipay = alipay_object()
    # 生成支付路由: 拼接url --> 返回url
    # 电脑网站支付，需要跳转到：https://openapi.alipay.com/gateway.do? + order_string
    order_string = alipay.api_alipay_trade_page_pay(
        # 这下面的数据，都应该是你数据库的数据，但是我这里做测试，直接写死了
        out_trade_no=goods_id,  # 商品订单号  唯一的
        total_amount=goods_price,  # 商品价格
        subject='购买' + goods_title + '待付款',  # 商品的名称
        # 同步回调网址--用于前端，付成功之后回调
        return_url=settings.ALIPAY_SETTING.get('ALIPAY_RETURN_URL'),
        # 异步回调网址---后端使用，post请求，网站未上线，post无法接收到响应内容,这里需要公网IP,本地测试无法调用使用该方式
        notify_url=settings.ALIPAY_SETTING.get('ALIPAY_NOTIFY_URL')
    )
    # 我这里大概讲一下为什么要有同步/异步，因为同步是前端的，
    # 如果前端出现页面崩了，那么校验由后端完成，
    # 而且在实际开发中，后端一定要校验，因为前端的校验，可被篡改
    url = 'https://openapi-sandbox.dl.alipaydev.com/gateway.do' + '?' + order_string
    return JsonResponse({'url': url, 'status': 200})





# def good_list_view(request):
#     if request.method == 'GET':
#         return render(request, 'good_list.html')
#     # 如果是post，我们认为是购买
#     # 示例化一个对象
#     alipay = alipay_obj()
#     # 生成支付路由
#     # 拼接url--返回url
#     # 电脑网站支付，需要跳转到：https://openapi.alipay.com/gateway.do? + order_string
#     order_string = alipay.api_alipay_trade_page_pay(
#         # 这下面的数据，都应该是你数据库的数据，但是我这里做测试，直接写死了
#         out_trade_no="20161123",  # 商品订单号  唯一的
#         total_amount=995,  # 商品价格
#         subject='商品支付',  # 商品的名称
#         return_url=settings.ALIPAY_SETTING.get('ALIPAY_RETURN_URL'),  # 同步回调网址--用于前端，付成功之后回调
#         notify_url=settings.ALIPAY_SETTING.get('ALIPAY_NOTIFY_URL')  # 异步回调网址---后端使用，post请求，网站未上线，post无法接收到响应内容
#     )
#     # 我这里大概讲一下为什么要有同步/异步，因为同步是前端的，
#     # 如果前端出现页面崩了，那么校验有后端完成，
#     # 而且在实际开发中，后端一定要校验，因为前端的校验，可被修改
#     url = 'https://openapi-sandbox.dl.alipaydev.com/gateway.do' + '?' + order_string
#     return JsonResponse({'url': url, 'status': 1})
