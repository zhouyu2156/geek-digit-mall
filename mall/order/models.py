from django.db import models
from user.models import User
from goods.models import Goods


class Order(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, verbose_name="订单ID")
    goods = models.CharField(max_length=32, verbose_name="商品ID")
    user = models.CharField(max_length=32, verbose_name="用户邮箱")
    goods_title = models.CharField(max_length=256, verbose_name="商品标题")
    user_name = models.CharField(max_length=32, verbose_name="下单用户")
    goods_url = models.CharField(max_length=256, verbose_name="商品主图链接")
    goods_price = models.IntegerField(null=False, verbose_name="商品单价")
    goods_count = models.IntegerField(default=0, verbose_name="订单数量")
    post_address = models.CharField(max_length=50, verbose_name="邮寄地址")
    logistics = models.CharField(max_length=256, verbose_name="物流信息")
    status = models.IntegerField(default=0, verbose_name="收货状态")

    class Meta:
        db_table = 'order'


class Membership(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
