import json
import time
from .models import Goods
from lxml import etree
from selenium import webdriver
from django.http import HttpResponse, JsonResponse
# 定义为常量,方便复用
from constant import script


# Create your views here.

def index(request):
    """:desc Goods Test..."""
    return HttpResponse('<h1 style="color: orangered;text-align:center;">Hell World! Goods 模块.</h1>')


def get_object(item):
    """:return 返回商品对象详情"""
    return {
        'id': item.id,
        'title': item.title,
        'price': item.price,
        'desc': item.desc,
        'total': item.total,
        'url': item.url,
        'img_url': item.img_url,
        'shop_name': item.shop_name,
        'sold_count': item.sold_count,
        'category': item.category,
        'status': item.status
    }
# 获取指定商品的详情信息
def get_goods_detail(request):
    ID = json.loads(request.body.decode('utf-8')).get('id')
    item = Goods.objects.get(id=ID)
    return HttpResponse(json.dumps(get_object(item)))
# 获取所有商品详情的列表
def get_goods_detail_list(request):
    """获取所有商品数据"""
    goods_queryset = Goods.objects.all()
    goods_list = []
    for item in goods_queryset:
        goods_list.append(get_object(item))
    return HttpResponse(json.dumps(goods_list))


def get_goods_detail_spider(request):
    """:description 获取商品详情数据"""
    # 通过自动化工具去淘宝获取商品的详情信息
    link = json.loads(request.body.decode(encoding='utf-8')).get('link')
    # 创建浏览器驱动对象
    browser = webdriver.Chrome()
    # 发起请求
    browser.get(link)
    # 等待1秒向下滑动
    time.sleep(1)
    # 定义浏览器执行脚本
    browser.execute_script(script)
    # 等待两秒获取源码
    time.sleep(2)
    tree = etree.HTML(browser.page_source)
    # 解析数据
    # 获取商品图片链接
    goods_img_url = 'https:' + tree.xpath('//img[@id="J_ImgBooth"]/@src')[0]
    # 获取商品标题
    goods_title = tree.xpath('//h3[@class="tb-main-title"]/text()')[0]
    # 获取商品价格
    goods_price = tree.xpath('//strong[@id="J_StrPrice"]/em[@class="tb-rmb-num"]/text()')[0]
    return JsonResponse({
        'details': {
            'img_url': goods_img_url,
            'title': goods_title,
            'price': goods_price
        }
    })
