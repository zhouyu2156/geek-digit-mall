# Generated by Django 5.0 on 2023-12-28 08:45

import uuid
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0012_alter_goods_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='goods',
            old_name='business',
            new_name='shop_name',
        ),
        migrations.AlterField(
            model_name='goods',
            name='id',
            field=models.CharField(auto_created=True, default=uuid.UUID('b9ca16be-cd0e-4458-851e-6ae9275d4b55'), max_length=32, primary_key=True, serialize=False, verbose_name='商品ID'),
        ),
    ]
