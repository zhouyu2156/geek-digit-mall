# Generated by Django 5.0 on 2023-12-26 16:58

import uuid
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0003_alter_goods_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='goods',
            name='name',
        ),
        migrations.AddField(
            model_name='goods',
            name='title',
            field=models.CharField(default=0, max_length=32, verbose_name='商品标题'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='goods',
            name='id',
            field=models.UUIDField(auto_created=True, default=uuid.uuid4, primary_key=True, serialize=False, verbose_name='商品ID'),
        ),
    ]
