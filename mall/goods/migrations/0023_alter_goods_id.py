# Generated by Django 5.0 on 2023-12-29 08:56

import uuid
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0022_alter_goods_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goods',
            name='id',
            field=models.CharField(auto_created=True, default=uuid.UUID('6d209a6b-054d-49c2-b048-b75eb5fd4052'), max_length=32, primary_key=True, serialize=False, verbose_name='商品ID'),
        ),
    ]
