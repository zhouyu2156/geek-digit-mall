# Generated by Django 5.0 on 2023-12-28 08:33

import uuid
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0007_goods_sold_count'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goods',
            name='category',
            field=models.CharField(max_length=32),
        ),
        migrations.AlterField(
            model_name='goods',
            name='id',
            field=models.UUIDField(auto_created=True, default=uuid.UUID('633998da-0178-4908-9b82-3254d20c57a1'), primary_key=True, serialize=False, verbose_name='商品ID'),
        ),
    ]
