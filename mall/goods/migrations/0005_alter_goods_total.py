# Generated by Django 5.0 on 2023-12-26 17:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0004_remove_goods_name_goods_title_alter_goods_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goods',
            name='total',
            field=models.IntegerField(default=100, verbose_name='商品库存数量'),
        ),
    ]
