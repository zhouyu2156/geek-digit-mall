from django.db import models
import uuid

class Goods(models.Model):
    """描述: 填写数据时, 尽量将全部字段填上, 这些字段都很重要, 前端展示需要"""
    id = models.CharField(max_length=32, default=uuid.uuid4(),primary_key=True,auto_created=True,verbose_name="商品ID")
    title = models.CharField(max_length=64, verbose_name="商品标题")
    price = models.CharField(max_length=256, verbose_name="商品价格")
    desc = models.CharField(max_length=256, verbose_name="商品描述")
    sold_count = models.CharField(max_length=20, verbose_name="真实销售量")
    total = models.IntegerField(default=100, verbose_name="商品库存数量")
    url = models.TextField(max_length=1024, verbose_name="商品链接")
    img_url = models.CharField(max_length=256, verbose_name="商品主图链接")
    url1 = models.CharField(max_length=256, verbose_name="商品轮播图链接1")
    url2 = models.CharField(max_length=256, verbose_name="商品轮播图链接2")
    url3 = models.CharField(max_length=256, verbose_name="商品轮播图链接3")
    shop_name = models.CharField(max_length=256, verbose_name="商家")
    category = models.CharField(max_length=32)
    status = models.SmallIntegerField(default=1)

    class Meta:
        db_table = 'goods'
















