from .models import Goods
from rest_framework import serializers


class GoodsModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Goods
        field = '__all__'
