from django.urls import path
from .views import *

urlpatterns = [
    path('', index),     # 模块首页视图
    path('get_goods_detail_list/', get_goods_detail_list),     # 获取商品详情
    path('get_goods_detail/', get_goods_detail),     # 获取商品详情
]
