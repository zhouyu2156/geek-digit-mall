import json
from django.http import HttpResponse, JsonResponse

def get_local_taobao_recommend(request):
    """获取本地首页推荐数据"""
    with open('taobao.json', 'r', encoding='utf-8') as fp:
        data = json.load(fp)
    return HttpResponse(json.dumps({'status': 200, 'msg': '请求成功', 'db': data}))


def get_local_category_goods_list(request):
    """获取本地分类数据"""
    kw = request.GET.get('kw')
    data = None
    if kw == '手机' or kw == '数码':
        with open(f'{kw}.json', 'r', encoding='utf-8') as fp:
            data = json.load(fp)
    else:
        return JsonResponse({'status': 400, 'msg': '请求数据有误!'})
    return HttpResponse(json.dumps({'status': 200, 'msg': '请求成功', 'db': data}))
