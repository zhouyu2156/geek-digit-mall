script = """
            const scrollHeight = document.documentElement.scrollHeight;
            const windowHeight = window.innerHeight;
            const scrollStep = Math.floor(scrollHeight / 100);
            let scrollPosition = 0;
            function scrollToBottom() {
                if (scrollPosition < scrollHeight) {
                    scrollPosition += scrollStep;
                    window.scrollTo(0, scrollPosition);
                    setTimeout(scrollToBottom, 50);
                }
            }
            scrollToBottom();
            """